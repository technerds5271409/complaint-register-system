import tkinter as tk
from tkinter import messagebox

complaints = []

def show_admin_page():
    admin_window = tk.Toplevel(root)
    admin_window.title("Admin Page")
    admin_label = tk.Label(admin_window, text="Welcome to the Administrative Page!")
    admin_label.place(x=10, y=10)

    complaint_listbox = tk.Listbox(admin_window)
    complaint_listbox.place(x=10, y=40)

    for complaint in complaints:
        complaint_listbox.insert(tk.END, complaint)

    def delete_complaint():
        selected_index = complaint_listbox.curselection()
        if selected_index:
            complaints.pop(selected_index[0])
            complaint_listbox.delete(selected_index)
    
    delete_button = tk.Button(admin_window, text="Delete", command=delete_complaint)
    delete_button.place(x=10, y=200)

def show_user_page():
    user_window = tk.Toplevel(root)
    user_window.title("User Page")

    name_label = tk.Label(user_window, text="Name:")
    name_label.place(x=10, y=10)
    name_entry = tk.Entry(user_window)
    name_entry.place(x=120, y=10)

    gender_label = tk.Label(user_window, text="Gender:")
    gender_label.place(x=10, y=40)
    gender_entry = tk.Entry(user_window)
    gender_entry.place(x=120, y=40)

    complaint_label = tk.Label(user_window, text="Complaint:")
    complaint_label.place(x=10, y=70)
    complaint_entry = tk.Entry(user_window)
    complaint_entry.place(x=120, y=70)

    def submit_complaint():
        user_name = name_entry.get()
        user_gender = gender_entry.get()
        user_complaint = complaint_entry.get()

        complaints.append(f"{user_name} ({user_gender}): {user_complaint}")
        name_entry.delete(0, tk.END)
        gender_entry.delete(0, tk.END)
        complaint_entry.delete(0, tk.END)

        messagebox.showinfo("Response Submitted", "Your response is submitted")
    
    submit_button = tk.Button(user_window, text="Submit", command=submit_complaint)
    submit_button.place(x=10, y=100)

def show_admin_login_page():
    admin_login_window = tk.Toplevel(root)
    admin_login_window.title("Admin Login")

    def validate_admin_login():
        entered_username = admin_username_entry.get()
        entered_password = admin_password_entry.get()

        if entered_username == "Jyoshna" and entered_password == "09":
            admin_login_window.destroy()
            show_admin_page()
        else:
            messagebox.showerror("Login Failed", "Invalid admin username or password")
    
    admin_username_label = tk.Label(admin_login_window, text="Username:")
    admin_username_label.place(x=10, y=10)
    admin_username_entry = tk.Entry(admin_login_window)
    admin_username_entry.place(x=120, y=10)

    admin_password_label = tk.Label(admin_login_window, text="Password:")
    admin_password_label.place(x=10, y=40)
    admin_password_entry = tk.Entry(admin_login_window, show="*")
    admin_password_entry.place(x=120, y=40)

    admin_login_button = tk.Button(admin_login_window, text="Login", command=validate_admin_login)
    admin_login_button.place(x=10, y=80)

def show_login_page(role):
    if role == "User":
        login_window = tk.Toplevel(root)
        login_window.title("User Login")

        def validate_login():
            entered_username = username_entry.get()
            entered_password = password_entry.get()

            if entered_username == "manju" and entered_password == "23":
                login_window.destroy()
                show_user_page()
            else:
                messagebox.showerror("Login Failed", "Invalid username or password")
        
        username_label = tk.Label(login_window, text="Username:")
        username_label.place(x=10, y=10)
        username_entry = tk.Entry(login_window)
        username_entry.place(x=120, y=10)

        password_label = tk.Label(login_window, text="Password:")
        password_label.place(x=10, y=40)
        password_entry = tk.Entry(login_window, show="*")
        password_entry.place(x=120, y=40)

        login_button = tk.Button(login_window, text="Login", command=validate_login)
        login_button.place(x=10, y=80)
    elif role == "Admin":
        show_admin_login_page()

def on_selection(selection):
    show_login_page(selection)

root = tk.Tk()
root.title("Role Selection")

selection_label = tk.Label(root, text="Select a role:")
selection_label.place(x=10, y=10)

admin_button = tk.Button(root, text="Administrative", command=lambda: on_selection("Admin"))
admin_button.place(x=120, y=10)

user_button = tk.Button(root, text="User", command=lambda: on_selection("User"))
user_button.place(x=220, y=10)

root.mainloop()