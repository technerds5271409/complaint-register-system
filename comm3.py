import tkinter as tk
from tkinter import messagebox
import sqlite3

# Create a database connection
conn = sqlite3.connect('complaints.db')
c = conn.cursor()

# Create users and admins tables
c.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY, username TEXT UNIQUE, password TEXT)''')

c.execute('''CREATE TABLE IF NOT EXISTS admins
             (id INTEGER PRIMARY KEY, username TEXT UNIQUE, password TEXT)''')

# Create complaints table
c.execute('''CREATE TABLE IF NOT EXISTS complaints
             (id INTEGER PRIMARY KEY, user_id INTEGER, complaint TEXT,
             FOREIGN KEY (user_id) REFERENCES users(id))''')

# Sample data for demonstration
c.execute("INSERT OR IGNORE INTO users (username, password) VALUES (?, ?)", ('Manju', '42'))
c.execute("INSERT OR IGNORE INTO users (username, password) VALUES (?, ?)", ('Yuga', '08'))
c.execute("INSERT OR IGNORE INTO users (username, password) VALUES (?, ?)", ('Jyoshna', '23'))
c.execute("INSERT OR IGNORE INTO users (username, password) VALUES (?, ?)", ('4501', '01'))
c.execute("INSERT OR IGNORE INTO admins (username, password) VALUES (?, ?)", ('Madhavi', 'mam'))

# Commit changes and close connection
conn.commit()
conn.close()

def show_admin_page():
    admin_window = tk.Toplevel(root)
    admin_window.title("Admin Page")
    admin_window.geometry("400x300")
    admin_window.configure(background='lightblue')  # Set background color of the admin window

    admin_label = tk.Label(admin_window, text="Welcome to the Administrative Page!", font=("Helvetica", 16), bg='lightblue')
    admin_label.pack(pady=20)

    # Listbox to display complaints
    complaint_listbox = tk.Listbox(admin_window, width=50, height=15)
    complaint_listbox.pack(pady=20)

    # Fetch complaints from the database and populate the listbox
    conn = sqlite3.connect('complaints.db')
    c = conn.cursor()
    c.execute("SELECT complaint FROM complaints")
    complaints = c.fetchall()
    conn.close()

    for complaint in complaints:
        complaint_listbox.insert(tk.END, complaint[0])

    def delete_complaint():
        # Delete selected complaint from the listbox and database
        selected_index = complaint_listbox.curselection()
        if selected_index:
            complaint = complaint_listbox.get(selected_index)
            conn = sqlite3.connect('complaints.db')
            c = conn.cursor()
            c.execute("DELETE FROM complaints WHERE complaint=?", (complaint,))
            conn.commit()
            conn.close()
            complaint_listbox.delete(selected_index)

    delete_button = tk.Button(admin_window, text="Delete", command=delete_complaint, bg='white')
    delete_button.pack()

    # Intercept close event to handle withdrawing the root window
    def on_close():
        root.deiconify()
        admin_window.destroy()

    admin_window.protocol("WM_DELETE_WINDOW", on_close)

def show_user_page():
    user_window = tk.Toplevel(root)
    user_window.title("User Page")
    user_window.geometry("400x300")
    user_window.configure(background='lightblue')  # Set background color of the user window

    # Intercept close event to handle withdrawing the root window
    def on_close():
        root.deiconify()
        user_window.destroy()

    user_window.protocol("WM_DELETE_WINDOW", on_close)

    name_label = tk.Label(user_window, text="Name:", font=("Helvetica", 14), bg='lightblue')
    name_label.pack(pady=10)
    name_entry = tk.Entry(user_window, font=("Helvetica", 14))
    name_entry.pack()

    gender_label = tk.Label(user_window, text="Gender:", font=("Helvetica", 14), bg='lightblue')
    gender_label.pack(pady=10)

    gender_var = tk.StringVar(value="Male")
    male_radio = tk.Radiobutton(user_window, text="Male", variable=gender_var, value="Male", font=("Helvetica", 12), bg='lightblue')
    male_radio.pack()
    female_radio = tk.Radiobutton(user_window, text="Female", variable=gender_var, value="Female", font=("Helvetica", 12), bg='lightblue')
    female_radio.pack()

    complaint_label = tk.Label(user_window, text="Complaint:", font=("Helvetica", 14), bg='lightblue')
    complaint_label.pack(pady=10)
    complaint_entry = tk.Entry(user_window, font=("Helvetica", 14))
    complaint_entry.pack()

    def submit_complaint():
        user_name = name_entry.get()
        user_gender = gender_var.get()
        user_complaint = complaint_entry.get()

        # Insert complaint into the database
        conn = sqlite3.connect('complaints.db')
        c = conn.cursor()
        c.execute("INSERT INTO complaints (user_id, complaint) VALUES (?, ?)", (1, f"{user_name} ({user_gender}): {user_complaint}"))
        conn.commit()
        conn.close()

        # Clear entry fields
        name_entry.delete(0, tk.END)
        complaint_entry.delete(0, tk.END)

        # Display confirmation message
        messagebox.showinfo("Response Submitted", "Your response is submitted")

    submit_button = tk.Button(user_window, text="Submit", command=submit_complaint, font=("Helvetica", 14), bg='white')
    submit_button.pack(pady=20)

def validate_login(username, password, role):
    conn = sqlite3.connect('complaints.db')
    c = conn.cursor()

    if role == "User":
        # Check if the entered credentials exist in the users table
        c.execute("SELECT * FROM users WHERE username=? AND password=?", (username, password))
        user = c.fetchone()
        conn.close()
        return user
    elif role == "Admin":
        # Check if the entered credentials exist in the admins table
        c.execute("SELECT * FROM admins WHERE username=? AND password=?", (username, password))
        admin = c.fetchone()
        conn.close()
        return admin
    else:
        conn.close()
        return None

def on_login():
    username = username_entry.get()
    password = password_entry.get()
    role = role_var.get()
    if role:
        user = validate_login(username, password, role)
        if user:
            messagebox.showinfo("Login Successful", f"Welcome, {username}!")
            if role == "User":
                show_user_page()
            elif role == "Admin":
                show_admin_page()
            root.withdraw()  # Hide the root window upon successful login
            username_entry.delete(0, tk.END)  # Clear username field after successful login
            password_entry.delete(0, tk.END)  # Clear password field after successful login
        else:
            messagebox.showerror("Login Failed", "Invalid username or password")
    else:
        messagebox.showerror("Role Selection", "Please select a role")

root = tk.Tk()
root.title("Complaint Registration System")
root.geometry("400x250")
root.configure(background='lightblue')  # Set background color of the root window

role_var = tk.StringVar(value="Admin")

role_label = tk.Label(root, text="Select Role:", font=("Helvetica", 14), bg='lightblue')
role_label.place(x=10, y=10)
user_radio = tk.Radiobutton(root, text="User", variable=role_var, value="User", font=("Helvetica", 12), bg='lightblue')
user_radio.place(x=120, y=10)
admin_radio = tk.Radiobutton(root, text="Admin", variable=role_var, value="Admin", font=("Helvetica", 12), bg='lightblue')
admin_radio.place(x=200, y=10)

username_label = tk.Label(root, text="Username:", font=("Helvetica", 14), bg='lightblue')
username_label.place(x=10, y=50)
username_entry = tk.Entry(root, font=("Helvetica", 14))
username_entry.place(x=120, y=50)

password_label = tk.Label(root, text="Password:", font=("Helvetica", 14), bg='lightblue')
password_label.place(x=10, y=90)
password_entry = tk.Entry(root, show="*", font=("Helvetica", 14))
password_entry.place(x=120, y=90)

login_button = tk.Button(root, text="Login", command=on_login, font=("Helvetica", 14), bg='white')
login_button.place(x=10, y=150)

root.mainloop()
