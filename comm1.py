import tkinter as tk
from tkinter import messagebox

complaints = []

def show_admin_page():
    admin_window = tk.Toplevel(root)
    admin_window.title("Admin Page")

    admin_label = tk.Label(admin_window, text="Welcome to the Administrative Page!")
    admin_label.pack()

    complaints_listbox = tk.Listbox(admin_window)
    complaints_listbox.pack()

    for complaint in complaints:
        complaints_listbox.insert(tk.END, complaint)

    def delete_complaint():
        selected_index = complaints_listbox.curselection()
        if selected_index:
            index = selected_index[0]
            complaints_listbox.delete(index)
            complaints.pop(index)

    delete_button = tk.Button(admin_window, text="Delete Complaint", command=delete_complaint)
    delete_button.pack()

def show_user_page():
    user_window = tk.Toplevel(root)
    user_window.title("User Page")

    name_label = tk.Label(user_window, text="Name:")
    name_label.pack()
    name_entry = tk.Entry(user_window)
    name_entry.pack()

    gender_label = tk.Label(user_window, text="Gender:")
    gender_label.pack()
    gender_entry = tk.Entry(user_window)
    gender_entry.pack()

    complaint_label = tk.Label(user_window, text="Complaint:")
    complaint_label.pack()
    complaint_entry = tk.Entry(user_window)
    complaint_entry.pack()

    def submit_complaint():
        user_name = name_entry.get()
        user_gender = gender_entry.get()
        user_complaint = complaint_entry.get()

        complaints.append(f"{user_name} ({user_gender}): {user_complaint}")

        name_entry.delete(0, tk.END)
        gender_entry.delete(0, tk.END)
        complaint_entry.delete(0, tk.END)

    submit_button = tk.Button(user_window, text="Submit", command=submit_complaint)
    submit_button.pack()

def on_selection(selection):
    if selection == "Admin":
        show_admin_page()
    elif selection == "User":
        show_user_page()

root = tk.Tk()
root.title("Role Selection")

selection_label = tk.Label(root, text="Select a role:")
selection_label.pack()

admin_button = tk.Button(root, text="Administrative", command=lambda: on_selection("Admin"))
admin_button.pack()

user_button = tk.Button(root, text="User", command=lambda: on_selection("User"))
user_button.pack()

root.mainloop()